# Automation priority: null
# Test case importance: Medium
# language: en
Feature: Add twice a product to the cart

	Scenario: Add twice a product to the cart
		Given I am logged in
		When I go to the Home page
		And I navigate to subcategory "Hommes"
		And I navigate to product "T-shirt imprimé colibri"
		And I add to cart
		And I go to the Home page
		And I navigate to subcategory "Hommes"
		And I navigate to product "T-shirt imprimé colibri"
		And I add to cart
		Then The cart contains
			      | Product                 | Number | Dimension | Size | Color |
			      | T-shirt imprimé colibri |      2 |           | S    | Blanc |
			